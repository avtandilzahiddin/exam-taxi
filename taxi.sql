1

CREATE TABLE Car (id BIGINT PRIMARY KEY, marka varchar(255), model varchar(255), toplivo varchar(255), obyem float, transmission varchar(255), prod_year varchar(255));
CREATE TABLE voditel (id BIGINT PRIMARY KEY, name varchar(255), surname varchar(255), birthdate varchar(255), driver_exp int, gender varchar(255), car INT REFERENCES car(id));
CREATE TABLE operator (id BIGINT PRIMARY KEY, name varchar(255), surname varchar(255), birthdate varchar(255), gender varchar(255));

2

INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (1, 'Toyota', 'Camry', 'Benzin', 2.5, 'avtomat', '2005');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (2, 'Toyota', 'Camry', 'Benzin', 2.5, 'mehanika', '2003');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (3, 'Toyota', 'Camry', 'Disel', 2, 'mehanika', '1999');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (4, 'Toyota', 'Corolla', 'Benzin', 1.6, 'avtomat', '2010');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (5, 'Toyota', 'Corolla', 'Benzin', 1.8, 'avtomat', '2009');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (6, 'Toyota', 'Corolla', 'Benzin', 1.8, 'mehanika', '2006');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (7, 'Mercedes', 'S-klass', 'Benzin', 4.4, 'avtomat', '2006');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (8, 'Mercedes', 'E-klass', 'Diesel', 3.3, 'avtomat', '2003');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (9, 'BMW', 'Semerka', 'Diesel', 3.0, 'avtomat', '2008');
INSERT INTO car(id, marka, model, toplivo, obyem, transmission, prod_year) VALUES (10, 'BMW', 'Troika', 'Benzin', 4.4, 'avtomat', '2012');

INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (1, 'Kanybek', 'Mametov', '1985-11-12', 10, 'male', 1);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (2, 'Kulumkan', 'Shaetova', '1999-11-12', 10, 'female', 2);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (3, 'Jainagul', 'Jolonova', '1990-12-09', 12, 'female', 3);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (4, 'Zarina', 'Ulusheva', '1993-11-09', 11, 'female', 4);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (5, 'Amina', 'Manasova', '1996-11-04', 13, 'female', 5);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (6, 'Azamat', 'Alymov', '1971-11-04', 9, 'male', 6);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (7, 'Kanybay', 'Akimov', '1983-11-04', 14, 'male', 7);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (8, 'Arstan', 'Alay', '1965-11-08', 30, 'male', 8);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (9, 'Maksat', 'Ulanov', '1988-06-08', 5, 'male', 9);
INSERT INTO voditel (id, name, surname, birthdate, driver_exp, gender, car) VALUES (10, 'Daniel', 'Ajiev', '1990-06-08', 7, 'male', 10);

INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (1, 'Azamat', 'Azamatov', '2000-05-05', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (2, 'Azamat', 'Ibraev', '1995-05-03', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (3, 'Myktybek', 'Arstanbek', '1966-04-03', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (4, 'Mirbek', 'Atabekov', '1988-02-07', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (5, 'Omar', 'Atabekov', '1999-02-07', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (6, 'Omar', 'Arstan', '1988-02-09', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (7, 'Mirbek', 'Arstan', '1991-03-09', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (8, 'Ulan', 'Damirov', '1985-11-09', 'male');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (9, 'Uuljan', 'Esenova', '1995-12-09', 'female');
INSERT INTO operator (id, name, surname, birthdate, gender) VALUES (10, 'Bubugul', 'Alimova', '2000-12-22', 'female');

3

SELECT * FROM car WHERE marka = 'Toyota' and model = 'Camry' ORDER BY prod_year DESC;

4

SELECT DISTINCT name FROM operator ORDER BY name DESC LIMIT 10;

5

UPDATE car SET marka = 'Mersus' WHERE marka = 'Mercedes';

6

DELETE FROM operator WHERE name = 'Azamat' and surname = 'Azamatov';

7

SELECT * FROM voditel WHERE driver_exp > 10 and gender = 'female';

8

SELECT AVG(DRIVER_EXP) FROM voditel WHERE birthdate > '1975-10-10';

9

SELECT voditel.id, voditel.name, voditel.surname, car.marka, car.model FROM voditel INNER JOIN car ON voditel.car = car.id;

10

SELECT marka, COUNT(id) FROM car GROUP BY marka ORDER BY count(id) DESC; 

11

SELECT name, COUNT(car) AS number_of_cars FROM voditel GROUP BY name ORDER BY count(car) DESC;





